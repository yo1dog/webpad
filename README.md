The ease and simplicity of Microsoft's Notepad on the web.

---------------------------------------

 - [View Website](http://webpad.awesomebox.net/)

---------------------------------------

[![screenshot](https://s3.amazonaws.com/mike-projects/WebPad/WebPadSC.png)](https://s3.amazonaws.com/mike-projects/WebPad/WebPadSC.png)