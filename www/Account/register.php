<?php
require("../header.php");

if($session->logged_in)
{
	header("Location: ../");
}
else if (isset($_SESSION['regsuccess']))
{
	if ($_SESSION['regsuccess'])
	{
?>
<h2>
	Registration Complete
</h2>
<p align="center">
	<br/>
    <br/>
    Your have successfully registered!<br/>
    <br/>
    You will be redirected to the home page in 3 seconds. Or click <a href="../">here</a> to go there immediately.
</p>
<script language=javascript>setTimeout("window.location = '<?php echo SITE_ROOT; ?>'", 3000)</script>
<?php
	}
	else
	{
?>
<h2>
	<span class="error">Registration Failed</span>
</h2>
<p>
	A problem occured while atempting to register your username "<?php echo $_SESSION['reguname']; ?>". Please try back later. If you continue to experience problems, please <a href="../about.php">contact</a> me with your username and any concerns.<br />
<?php
	}
	
	unset($_SESSION['regsuccess']);
  	unset($_SESSION['reguname']);
}
else
{
?>
<h2>
Create a New Account
</h2>
<p>
	Use the form below to create a new account.
</p>
<p>
	Passwords are required to be a minimum of 6 characters in length.
</p>
<br />
<div class="accountInfo">
	<form action="../Database/process.php" method="POST">
        <fieldset class="login">
            <legend>Account Information</legend>
            <p>
                User Name:
                <input type="text" name="user" class="textEntry" maxlength="30" value="<?php echo $form->value("user"); ?>" />
                <span class="failureNotification"><?php echo $form->error("user"); ?></span>
            </p>
            <p>
                E-mail:
                <input type="text" name="email" class="textEntry" maxlength="30" value="<?php echo $form->value("email"); ?>" />
                <span class="failureNotification"> <?php echo $form->error("email"); ?></span>
            </p>
            <p>
                Password:
                <input type="password" name="pass" class="textEntry" maxlength="30" value="" />
                <span class="failureNotification"> <?php echo $form->error("pass"); ?></span>
            </p>
            <p>
                Confirm Password:
                <input type="password" name="confirmpass" class="textEntry" maxlength="30" value="" />
                <span class="failureNotification"> <?php echo $form->error("confirmpass"); ?></span>
            </p>
        </fieldset>
        <p class="submitButton">
    		<input type="hidden" name="subjoin" value="1">
			<input type="submit" value="Create User">
        </p>
    </form>
</div>

<?php
}

include("../footer.php");
?>