<?php
function head($title)
{
	return
'<html>
<head>
	<title>' . htmlspecialchars($title) . '</title>
	<link id="Link1" runat="server" rel="shortcut icon" href="Images/favicon.ico" type="image/x-icon" />
	<link id="Link2" runat="server" rel="icon" href="Images/favicon.ico" type="image/ico" />
	<link href="Styles/Site.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<form>
	<div class="page">
		<div class="header">
			<div class="title">
				<h1>
					WebPad
				</h1>
			</div>
			<div class="loginDisplay">
				[ Log In ]
			</div>
			<div class="clear hideSkiplink">
			</div>
		</div>
		<div class="main">';
}

function foot()
{
	return
		'</div>
		<div class="clear">
		</div>
		<div class="footer">
			Footer Text
		</div>
    </form>
</body>
</html>';
}
?>