<?php
include_once("session.php");
include_once("rootconstants.php");

if (!$session->logged_in)
	showError("User is not logged in.");

$req_user_table = $database->getUserPageTable($session->username);

if ($req_user_table == NULL)
{
	$result = $database->createUserPageTable($session->username);
	if ($result)
	{
		$req_user_table = $database->getUserPageTable($session->username);
		
		if ($req_user_table == NULL)
		{
			showError("A problem has occured while atempting to create and connect to your table.","Created the table \"" . TBL_USERPAGE_PREFIX . $session->username . "\" but failed to connect to it.");
		}
	}
	else
		showError("A problem has occured while atempting to create and connect to your table.","Failed to connect to the table \"" . TBL_USERPAGE_PREFIX . $session->username . "\" and failed to create it.");
}
?>

<table cellspacing="0" class="pages">
	<tr valign="top" class="pheader">
    	<th scope="col"></th>
        <th scope="col"></th>
        <th scope="col"></th>
        <th scope="col"></th>
        <th scope="col" class="ColTitle">Title</th>
        <th scope="col" class="ColPrivate">P</th>
        <th scope="col" class="ColPublicEdit">PE</th>
        <th scope="col" class="ColDate">Last Edited</th>
    </tr>
<?php
$alternate = false;
while ($row=mysql_fetch_array($req_user_table))
{
?>
	<tr onmouseover="javascript:PagesTable_Row_MouseOver('<?php echo $row['ID']; ?>');"
    	onmouseout="javascript:PagesTable_Row_MouseOut('<?php echo $row['ID']; ?>');"
	<?php
		if ($alternate)
		{
			echo "class=\"alternate\"";
			$alternate = false;
		}
		else
			$alternate = true;
	?>>
    	<td class="ColCheckbox">
        	<input id="Checkbox_<?php echo $row['ID']; ?>" name="Checkbox_<?php echo $row['ID']; ?>" type="checkbox" />
            <script language="javascript">addCheckbox("<?php echo $row['ID']; ?>")</script>
        </td>
        <td class="ColDelete">
        	<input id="ImageButtonDelete_<?php echo $row['ID']; ?>" name="ImageButtonDelete_<?php echo $row['ID']; ?>" type="image" src="Images/delete_12x12_fade.png" onclick="procDelete('<?php echo $row['ID']; ?>')" />
        </td>
        <td class="ColLink">
        	<input id="ImageButtonLink_<?php echo $row['ID']; ?>" name="ImageButtonLink_<?php echo $row['ID']; ?>" type="image" src="Images/link_14x14_fade.png" onclick="showLinks('<?php echo $row['ID']; ?>')" />
        </td>
        <td class="ColRename">
        	<input id="ImageButtonRename_<?php echo $row['ID']; ?>" name="ImageButtonRename_<?php echo $row['ID']; ?>" type="image" src="Images/rename_16x14_fade.png" onclick="procRename('<?php echo $row['ID']; ?>')" />
        </td>
        <td class="ColTitle" nowrap>
        	<a id="LinkTitle_<?php echo $row['ID']; ?>" href="editpage.php?pageid=<?php echo $row['ID']; ?>"><?php echo printTitle($row['Title']); ?></a>
        </td>
        <td class="ColPrivate">
        	<input id="CheckboxPrivate_<?php echo $row['ID']; ?>" name="CheckboxPrivate_<?php echo $row['ID']; ?>" type="checkbox" disabled="true" <?php if(!$row['Private']) echo("checked"); ?> />
        </td>
        <td class="ColPublicEdit">
        	<input id="CheckboxPublicEdit_<?php echo $row['ID']; ?>" name="CheckboxPublicEdit_<?php echo $row['ID']; ?>" type="checkbox" disabled="true" <?php if($row['PublicEdit']) echo("checked"); ?> />
        </td>
        <td class="ColDate">
        	<?php echo formatDateTime($row['EDate']); ?>
        </td>
    </tr>
<?php
}
?>
</table>