<?php
include("header.php");

if (!$session->logged_in)
	showError("User is not logged in.");

$pageid = $_POST['editPageID'];

if (!$pageid)
	showError("Unable to determain what pages to rename.","Post \"pageid\" is empty");

if ($pageid[strlen($pageid) - 1] === ",")
	$pageid = substr($pageid, 0, strlen($pageid) - 1);

$req_user_table = $database->getUserPageTableWhere($session->username, $pageid);
if($req_user_table == NULL)
{
	showError("A problem has occured while atempting to connect to your table.","Failed to connect to table \"" . TBL_USERPAGE_PREFIX . $session->username . "\".");
}
?>

<h2>
	Rename
</h2>
<p>
    <form action="Database/process.php" method="POST">
        <table class="rename">
<?php
		while ($row = mysql_fetch_array($req_user_table))
		{
?>
            <tr>
                <td>
                    <?php echo $row['Title']; ?>
                </td>
                <td class="date">
                	<?php echo formatDateTime($row['EDate']); ?>
                </td>
                <td>
                    <input type="text" name="Textbox_<?php echo $row['ID']; ?>" id="Textbox_<?php echo $row['ID']; ?>" class="textEntryRename" />
                </td>
            </tr>
<?php
		}
?>
        </table>
        <br />
        <input type="hidden" name="editAction" value="rename" />
        <input type="hidden" name="editPageID" value="<?php echo $pageid; ?>" />
        <input type="hidden" name="subedittable" value="1" />
        <input type="submit" value="Rename" />
	</form>
</p>

<?php
include("footer.php");
?>