<?php
require("header.php");

if($session->logged_in)
{
?>
<form action="Database/process.php" id="mainForm" method="POST" onSubmit="return formVerify()">
	<p>
    	<input type="submit" style="visibility:hidden; width:1px; height:1px;" onclick="determainAction()" />
		<?php include("Database/displaytables.php"); ?>
    </p>
    <p>
        <input type="submit" value="Rename" onclick="procRename('')" />&nbsp;&nbsp;&nbsp;<input type="submit" value="Delete" onclick="procDelete('')" />
    </p>
    <p>
        <input type="text" id="title" name="title" class="textEntryNew" maxlength="30" onfocus="this.focused=true;" onblur="this.focused=false;" />&nbsp;&nbsp;&nbsp;<input type="submit" value="New" onclick="procNew(true)" />
    </p>
    <input type="hidden" name="subedittable" value="1" />
    <input type="hidden" name="date" id="date" value="" />
    <input type="hidden" id="editAction" name="editAction" value="" />
    <input type="hidden" id="editPageID" name="editPageID" value="" />
</form>

<?php
}
else
{
?>

<h2>
Log In
</h2>
<p>
Please enter your username and password. <a href="Account/register.php">Register</a> if you don't have an account.
</p>
<br />
<center>
<div class="accountInfo">
	<form action="Database/process.php" method="POST">
        <fieldset class="login">
            <legend>Account Information</legend>
            <p>
                Username:
                <input type="text" name="user" class="textEntry" maxlength="30" value="<?php echo $form->value("user"); ?>" />
                <span class="failureNotification"><?php echo $form->error("user"); ?></span>
            </p>
            <p>
                Password:
                <input type="password" name="pass" class="textEntry" maxlength="30" value="" />
                <span class="failureNotification"> <?php echo $form->error("pass"); ?></span>
            </p>
        </fieldset>
        <p class="submitButton">
    		<input type="hidden" name="sublogin" value="1">
			<input type="submit" value="Login">
        </p>
    </form>
</div>
</center>

<div>
	<h2>Popular</h2>
	<ul>
		<li><a href="/viewpage.php?username=yo1dog&pageid=10">Want List - yo1dog</a></li>
		<li><a href="/viewpage.php?username=yo1dog&pageid=21">Christmas List - yo1dog</a></li>
	</ul>  
</div>

<?php
}

include("footer.php");
?>
