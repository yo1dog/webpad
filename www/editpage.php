<?php
require("header.php");

$pageid = isset($_GET["pageid"])? $_GET["pageid"] : NULL;
$username = isset($_GET["username"])? $_GET["username"] : NULL;
$public = true;
$publicEdit = false;
$editable = true;
$new = false;

if (!$session->logged_in)
{
	if ($username == NULL)
		showError("A problem has occured while atempting to retrieve the Page data.","Unable to identify user. User is not logged in and the URL paramater \"username\" was not found.");
}
else
	$username = $session->username;

if ($pageid == NULL)
{
	showError("A problem has occured while atempting to retrieve the Page data.","URL paramater \"pageid\" was not found.");
}

if (!is_numeric($pageid))
{
	showError("A problem has occured while atempting to retrieve the Page data.","Page ID \"" . $pageid . "\" is not a valid integer.");
}

if ($pageid == -1)
{
	if ($username !== $session->username)
	{
		showError("A problem has occured while atempting to retrieve the Page data.","Cannot use Page ID of -1 unless creating a new page for the logged in user.");
	}
	else
		$new = true;
}

if (!$new)
{	
	$req_page_row = $database->getUserPage($username, $pageid);
	
	if ($req_page_row == NULL)
		showError("A problem has occured while atempting to retrieve the Page data.","Unable to locate Page ID \"" . $pageid . "\".");
	
	$row = mysql_fetch_array($req_page_row);

	if($row['Private'])
		$public = false;
	if ($row['PublicEdit'])
		$publicEdit = true;
	
	if ($username !== $session->username)
	{
		if (!$public)
			showError("You do not have permission to access this Page.","The owner of this Page has set it to Private.");
		
		if (!$publicEdit)
			$editable = false;
	}
}
?>
<script language="javascript">
var changed = false;
var saving = false;
window.onbeforeunload=customBodyUnload;

function customBodyUnload(e)
{
	if (!saving)
	{
		if (document.getElementById("CheckboxPublic").checked != <?php if ($public) echo "true"; else echo "false"; ?>)
			changed = true;
		else if (document.getElementById("CheckboxPublicEdit").checked != <?php if ($publicEdit) echo "true"; else echo "false"; ?>)
			changed = true;
		
		if (changed || <?php if($new) echo "true"; else echo "false"; ?>)
			return "Any unsaved changes will be lost.";
	}
}
</script>

<form action="Database/process.php" method="POST">
    <table style="width:100%; font-size:inherit;">
        <tr id="titleContainer">
            <td>
                <h2>
                	<input type="text" id="title" name="title" class="textEntryTitle" maxlength="30" value="<?php if ($new) echo printTitle($_POST['title']); else echo printTitle($row['Title']); ?>" onfocus="dynamicTextboxOnFocus(this);" onblur="dynamicTextboxOnBlur(this, document.getElementById('spanTitle'), document.getElementById('titleContainer'));" onmouseover="dynamicTextboxOnMouseOver(this);" onmouseout="dynamicTextboxOnMouseOut(this);" onchange="changed=true;" autocomplete="off" /><span id="spanTitle" style="position:absolute; top:0; left:0;"></span>
                </h2>
            </td>
            <td id="titleBuffer" class="date">
                <?php
				if (!$new)
					echo formatDateTime($row['EDate']);
				?>
            </td>
            <script language="javascript">dynamicTextboxOnCreate('title', 'spanTitle', 'titleContainer', 'titleBuffer');</script>
        </tr>
        <tr>
            <td align="right" colspan="2">
                <b><label for="CheckboxPublic">Public</label><input name="CheckboxPublic" id="CheckboxPublic" type="checkbox" <?php if($public) echo("checked"); ?> <?php if(!$editable) echo("disabled"); ?> />&nbsp;<label for="CheckboxPublicEdit">Public Edit</label><input name="CheckboxPublicEdit" id="CheckboxPublicEdit" type="checkbox" <?php if($publicEdit) echo("checked"); ?> <?php if(!$editable) echo("disabled"); ?> /></b>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <textarea name="content" rows="20" class="editPage" onchange="changed=true;" id="content"><?php if (!$new) echo $row['Content']; ?></textarea>
            </td>
        </tr>
        <?php
		if ($editable)
		{
		?>
        <tr>
            <td colspan="2">
            	<input type="hidden" name="subeditpage" value="1" />
                <input type="hidden" name="pageid" value="<?php echo $pageid; ?>" />
                <input type="hidden" id="date" name="date" value="" />
                <input type="submit" value="Save" onclick="setDateTime(); saving=true;" />&nbsp;&nbsp;&nbsp;<input type="button" value="Cancel" onclick="window.location = '<?php echo SITE_ROOT; ?>'" />
                <?php
                	if (isset($_SESSION['editpage']))
					{
						if ($_SESSION['editpage'])
						{
							echo "&nbsp;&nbsp;&nbsp;<span id=\"savedMessage\" class=\"successNotification\">Saved</span>";
							echo "<script language=javascript>startfade(\"savedMessage\", 1500, 0.02, 10)</script>";
						}
						else
						{
							global $database;
							echo "&nbsp;&nbsp;&nbsp;<span class=\"failureNotification\">Save Failed</span>";
						}
						
						unset($_SESSION['editpage']);
					}
				?>
            </td>
        </tr>
        <?php
		}
		?>
    </table>
</form>

<?php
if (!$new)
{
	?>

<div style="text-align:right">
	<a href="/viewpage.php?username=<?php echo $username; ?>&pageid=<?php echo $pageid; ?>">View Page</a>
</div>

	<?php
}
?>

<script type="text/javascript">

    document.getElementById("content").addEventListener('keydown', keyHandler, false);

    function keyHandler(e)
    {
        if(e.keyCode == 9)
        {
            var pos = this.selectionStart;
            this.value = this.value.substring(0, pos) + "\t" + this.value.substring(pos);
            this.setSelectionRange(pos + 1, pos + 1);

            e.preventDefault();
            return false;
        }
    }
</script>
<?php

include("footer.php");
?>
