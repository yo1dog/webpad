<?php
require("header.php");

$pageid = isset($_GET["pageid"])? $_GET["pageid"] : NULL;
$username = isset($_GET["username"])? $_GET["username"] : NULL;

if (!$session->logged_in)
{
	if ($username == NULL)
		showError("A problem has occured while atempting to retrieve the Page data.","Unable to identify user. User is not logged in and the URL paramater \"username\" was not found.");
}
else
{
	$username = $session->username;
}

if ($database->getUserPageTable($username) == NULL)
{
	showError("A problem has occured while atempting to retrieve the Page data.","Failed to connect to table \"" . TBL_USERPAGE_PREFIX . $username . "\".");
}

else if ($pageid == NULL)
{
	showError("A problem has occured while atempting to retrieve the Page data.","URL paramater \"pageid\" was not found.");
}

else if (!is_numeric($pageid))
{
	showError("A problem has occured while atempting to retrieve the Page data.","Page ID \"" . $pageid . "\" is not a valid integer.");
}
else
{
	$req_page_row = $database->getUserPage($username, $pageid);
	if ($req_page_row == NULL)
	{
		showError("A problem has occured while atempting to retrieve the Page data.","Unable to locate Page ID \"" . $pageid . "\".");
	}
	else
	{
		$row = mysql_fetch_array($req_page_row);
		
		if ($username !== $session->username)
		{
			if ($row['Private'])
				showError("You do not have permission to access this Page.","The owner of this Page has set it to Private.");
		}
?>

<script type="text/javascript">
var largeImages = false;

function toggleLargeImages() {
  largeImages = !largeImages;
  var klass = largeImages? "view-item-image-cell-large" : "view-item-image-cell";
  
  var imageCells = document.getElementsByName("image-cell");
  for (var i = 0; i < imageCells.length; ++i) {
    imageCells[i].className = klass;
  }
}
</script>

<div style="text-align:right">
        <a href="javascript:toggleLargeImages()">Toggle Images</a><br />

<?php
if ($row['PublicEdit'] || $username === $session->username)
{
	?>
        <a href="/editpage.php?pageid=<?php echo $pageid; ?>">Edit Page</a>
	<?php
}
?>

</div>



<table style="width:100%; font-size:small;">
    <tr>
        <td>
            <h2>
                <?php echo $row['Title']; ?>
            </h2>
        </td>
        <td style="text-align:right; vertical-align:bottom; width:150px;">
            <?php echo formatDateTime($row['EDate']); ?>
        </td>
    </tr>
    <tr>
        <td colspan="2">
        	<hr />
            <table style="width:100%; font-size:normal;">
            	<?php
            	$lines = explode("\n", $row['Content']);
            	$lines[] = ""; // always add an empty ending line so the last line does not get cut off
                
            	if (is_array($lines))
            	{
            		$lastLineItemHTML = null;
            		
            		foreach ($lines as $line)
            		{
            			if (strlen(trim($line)) === 0)
            			{
            				if ($lastLineItemHTML !== null)
            				{
            					if ($lastLineItemHTML[0] === '-')
            					{
            						$lastLineItemHTML = ltrim(ltrim($lastLineItemHTML, '-'));
            						echo '<tr><td colspan="2">', $lastLineItemHTML, '</td></tr>';
            					}
            					else
            						echo '<tr><td></td><td>', $lastLineItemHTML, '</td></tr>';
            				}
            				
            				echo '<tr><td colspan="2">&nbsp;</td></tr>';
            				$lastLineItemHTML = null;
            				continue;
            			}
            			
            			$isImageURL = $line[0] === "\t" && $lastLineItemHTML !== null;
            			
            			$line = trim($line);
            			
            			if ($isImageURL)
            			{
            				echo '<tr><td name="image-cell" class="view-item-image-cell"><img src="', $line, '"/></td><td>', $lastLineItemHTML, '</td></tr>';
            				$lastLineItemHTML = null;
            				continue;
            			}
            			
            			if ($lastLineItemHTML !== null)
            			{
            				if ($lastLineItemHTML[0] === '-')
            				{
            					$lastLineItemHTML = ltrim(ltrim($lastLineItemHTML, '-'));
            					echo '<tr><td colspan="2">', $lastLineItemHTML, '</td></tr>';
            				}
            				else
            					echo '<tr><td></td><td>', $lastLineItemHTML, '</td></tr>';
            				
            				$lastLineItemHTML = null;
            			}
            			
            			$isSeperator = true;
            			for ($i = 0; $i < strlen($line); $i++)
            			{
            				if ($line[$i] !== '-')
            				{
            					$isSeperator = false;
            					break;
            				}
            			}
            			
            			if ($isSeperator)
            			{
            				echo '<tr><td colspan="2"><hr style="border-style: dashed none none none;" /></td></tr>';
            				continue;
            			}
            			
            			$title = null;
            			$url = null;
            			
            			$firstTabIndex = strpos($line, "\t");
            			
            			if ($firstTabIndex === false)
            				$title = $line;
            			else
            			{
            				$lastTabIndex = strrpos($line, "\t");
            				
            				$title = substr($line, 0, $firstTabIndex);
            				$url = substr($line, $lastTabIndex + 1);
            			}
            			
            			$lastLineItemHTML = '';
            			
            			if ($url !== null)
            				$lastLineItemHTML .= '<a href="' . htmlspecialchars($url) . '" target="_blank">';
            			
            			$lastLineItemHTML .= htmlspecialchars($title);
            			
            			if ($url !== null)
            				$lastLineItemHTML .= '</a>';
            		}
            	}
            	?>
            </table>
            <hr />
        </td>
    </tr>
</table>

<?php
	}
}

include("footer.php");
?>
