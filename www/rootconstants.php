<?php
include_once("Database/session.php");

define("SITE_ROOT", "/");

define("_TIMEZONES", '<option value="-12">(UTC-12:00) Dateline Standard Time</option><option value="-11">(UTC-11:00) Samoa Standard Time</option><option value="-10">(UTC-10:00) Hawaiian Standard Time</option><option value="-9">(UTC-09:00) Alaskan Standard Time</option><option value="-8">(UTC-08:00) Pacific Standard Time</option><option value="-7">(UTC-07:00) Mexican Standard Time[La Paz]</option><option value="-7">(UTC-07:00) Mountain Standard Time</option><option value="-7">(UTC-07:00) Mountain Standard Time [Arizona]</option><option value="-6">(UTC-06:00) Central Standard Time</option><option value="-6">(UTC-06:00) Mexico Standard Time</option><option value="-6">(UTC-06:00) Canada Central Standard Time</option><option value="-6">(UTC-06:00) Central America Standard Time</option><option value="-5">(UTC-05:00) Eastern Standard Time</option><option value="-5">(UTC-05:00) SA Pacific Standard Time</option><option value="-4">(UTC-04:00) Atlantic Standard Time</option><option value="-4">(UTC-04:00) SA Western Standard Time</option><option value="-4">(UTC-04:00) Pacific SA Standard Time</option><option value="-3.5">(UTC-03:30) Newfoundland Standard Time</option><option value="-3.5">(UTC-03:30) SA Eastern Standard Time</option><option value="-3">(UTC-03:00) E. South America Standard Time</option><option value="-3">(UTC-03:00) Greenland Standard Time</option><option value="-2">(UTC-02:00) Mid-Atlantic Standard Time</option><option value="-1">(UTC-01:00) Azores Standard Time</option><option value="-1">(UTC-01:00) Cape Verde Standard Time</option><option value="0">(UTC) Universal Coordinated Time</option><option value="0">(UTC) GMT</option><option value="1">(UTC+01:00) Romance Standard Time</option><option value="1">(UTC+01:00) W. Central Africa Standard Time</option><option value="1">(UTC+01:00) Central European Standard Time</option><option value="1">(UTC+01:00) W. Europe Standard Time</option><option value="2">(UTC+02:00) Egypt Standard Time</option><option value="2">(UTC+02:00) South Africa Standard Time</option><option value="2">(UTC+02:00) Israel Standard Time</option><option value="2">(UTC+02:00) E. Europe Standard Time</option><option value="2">(UTC+02:00) FLE Standard Time</option><option value="2">(UTC+02:00) GTB Standard Time</option><option value="3">(UTC+03:00) Arab Standard Time</option><option value="3">(UTC+03:00) E. Africa Standard Time</option><option value="3">(UTC+03:00) Arabic Standard Time</option><option value="3">(UTC+03:00) Russian Standard Time</option><option value="3.5">(UTC+03:30) Iran Standard Time</option><option value="4">(UTC+04:00) Arabian Standard Time</option><option value="4">(UTC+04:00) Caucasus Standard Time</option><option value="4">(UTC+04:00) Afghanistan Standard Time</option><option value="5">(UTC+05:00) West Asia Standard Time</option><option value="5">(UTC+05:00) Ekaterinburg Standard Time</option><option value="5.5">(UTC+05:30) India Standard Time</option><option value="5.75">(UTC+05:45) Nepal Standard Time</option><option value="6">(UTC+06:00) Central Asia Standard Time</option><option value="6">(UTC+06:00) Sri Lanka Standard Time</option><option value="6">(UTC+06:00) N. Central Asia Standard Time</option><option value="6.5">(UTC+06:30) Myanmar Standard Time</option><option value="7">(UTC+07:00) SE Asia Standard Time</option><option value="7">(UTC+07:00) North Asia Standard Time</option><option value="8">(UTC+08:00) China Standard Time</option><option value="8">(UTC+08:00) W. Australia Standard Time</option><option value="8">(UTC+08:00) Singapore Standard Time</option><option value="8">(UTC+08:00) Taipei Standard Time</option><option value="8">(UTC+08:00) North Asia East Standard Time</option><option value="9">(UTC+09:00) Tokyo Standard Time</option><option value="9">(UTC+09:00) Korea Standard Time</option><option value="9">(UTC+09:00) Yakutsk Standard Time</option><option value="9.5">(UTC+09:30) AUS Central Standard Time</option><option value="9.5">(UTC+09:30) Cen. Australia Standard Time</option><option value="10">(UTC+10:00) AUS Eastern Standard Time</option><option value="10">(UTC+10:00) E. Australia Standard Time</option><option value="10">(UTC+10:00) West Pacific Standard Time</option><option value="10">(UTC+10:00) Tasmania Standard Time</option><option value="10">(UTC+10:00) Vladivostok Standard Time</option><option value="11">(UTC+11:00) Central Pacific Standard Time</option><option value="12">(UTC+12:00) Fiji Standard Time</option><option value="12">(UTC+12:00) New Zealand Standard Time</option><option value="13">(UTC+13:00) Tonga Standard Time</option>');

function showError($message, $details)
{
	if (strlen($message) == 0)
		$message = "A problem has occured.";
		
	$result = "<h2><error>Error</error></h2><br />" . $message . " Please try back later. If you continue to experience problems, please <a href=\"about.php\">contact</a> me with your username and any concerns.";
	
	if (strlen($details) > 0)
	{
		$result = $result . "<br /><br /><b>Additional Details:</b><br />" . $details;
		
		if (strlen(mysql_error()) > 0)
			$result = $result . "<br /><br />" . mysql_error();
	}
	else if (isset($database) && strlen(mysql_error($database->connection)) > 0)
		$result = $result . "<br /><br /><b>Additional Details:</b><br />" . mysql_error();
	
	die($result);
	/*$_SESSION['errorPageContent'] = $result;
	echo("<script language=\"javascript\">window.location = \"" . SITE_ROOT . "errorpage.php\"</script>");*/
}

function formatTitle($title)
{
	return str_replace("'", "\\'", $title);
}

function printTitle($title)
{
	return str_replace('"', "&quot;", $title);
}

function formatDateTime($datetime)
{
	return date_format(date_create($datetime, new DateTimeZone('America/Chicago')), "n/j/y g:i:s A");
}
?>
