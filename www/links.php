<html>
    <head>
		<title>WebPad</title>
        <link id="Link1" runat="server" rel="shortcut icon" href="Images/favicon.ico" type="image/x-icon" />
        <link id="Link2" runat="server" rel="icon" href="Images/favicon.ico" type="image/ico" />
        <link href="Styles/Site.css" rel="stylesheet" type="text/css" />
        
        <script type="text/javascript">
			window.screenX = (window.opener.screenX + window.opener.outerWidth/2) - 150;
			window.screenY = (window.opener.screenY + window.opener.outerHeight/2) - 70;
			window.focus();
		</script>
    </head>
    <body class="links">
    	<fieldset class="links">
            	&nbsp;&nbsp;Below is the public link to your page. Users will still not be able to view your page if it is marked as private. <br />
                <br />
                <br />
            	<input type="text" class="textDisplayLink" readonly="readonly" value="<?php include("rootconstants.php"); echo SITE_ROOT . "editpage.php?username=" . $_GET['username'] . "&pageid=" . $_GET['pageid']; ?>"  />
        </fieldset>
    </body>
</html>