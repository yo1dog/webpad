<?php
include_once("Database/session.php");
include_once("rootconstants.php");
?>

<html>
    <head>
        <title>WebPad</title>
        <link id="Link1" runat="server" rel="shortcut icon" href="<?php echo SITE_ROOT; ?>Images/favicon.ico" type="image/x-icon" />
        <link id="Link2" runat="server" rel="icon" href="<?php echo SITE_ROOT; ?>Images/favicon.ico" type="image/ico" />
        <link href="<?php echo SITE_ROOT; ?>Styles/Site.css" rel="stylesheet" type="text/css" />
        
        <script type="text/javascript">
			var checkboxes = new Array();
			var dynamicTextboxes = new Array();
			 
			function PagesTable_Row_MouseOver(pageID) {
				ImageButtonDelete = document.getElementById("ImageButtonDelete_" + pageID);
				ImageButtonLink = document.getElementById("ImageButtonLink_"  + pageID);
				ImageButtonRename = document.getElementById("ImageButtonRename_" + pageID);

				ImageButtonDelete.src = 'Images/delete_12x12.png';
				ImageButtonLink.src = 'Images/link_14x14.png';
				ImageButtonRename.src = 'Images/rename_16x14.png';
			}
		
			function PagesTable_Row_MouseOut(pageID) {
				ImageButtonDelete = document.getElementById("ImageButtonDelete_" + pageID);
				ImageButtonLink = document.getElementById("ImageButtonLink_"  + pageID);
				ImageButtonRename = document.getElementById("ImageButtonRename_" + pageID);
		
				ImageButtonLink.src = 'Images/link_14x14_fade.png';
				ImageButtonDelete.src = 'Images/delete_12x12_fade.png';
				ImageButtonRename.src = 'Images/rename_16x14_fade.png';
			}
			
			function determainAction()
			{
				if (document.getElementById("title").focused)
					procNew();
			}
			
			
			function startfade(elementID, idelay, inc, delay)
			{
				setTimeout("fade(\"" + elementID + "\", 1.0, " + inc + ", " + delay + ")", idelay);
			}
			
			function fade(elementID, value, inc, delay)
			{
				value -= inc;
				if (value < 0)
					value = 0;
				
				element = document.getElementById(elementID);
				element.style.opacity = value;
				
				if (value > 0)
					setTimeout("fade(\"" + elementID + "\", " + value + ", " + inc + ", " + delay + ")", delay);
			}
			
			
			function setDateTime(utc, easyread)
			{
				var time = new Date();
				
				var year = "" + time.getFullYear();
				var month;
				var day;
				var hour;
				var minute;
				var oMinute;
				var second;
				var oSecond;
				var suffix = "AM";
				
				if (utc)
				{
					month = time.getUTCMonth();
					day = time.getUTCDate();
					hour = time.getUTCHours();
					minute = time.getUTCMinutes();
					second = time.getUTCSeconds();
				}
				else
				{
					month = time.getMonth();
					day = time.getDate();
					hour = time.getHours();
					minute = time.getMinutes();
					second = time.getSeconds();

					if (easyread)
					{
						if (hour >= 12)
							suffix = "PM";
						if (hour > 12)
							hour -= 12;
					}
				}
				
				if (minute < 10)
					oMinute = "0" + minute;
				else
					oMinute = minute;
					
				if (second < 10)
					oSecond = "0" + second;		
				else
					oSecond = second;
				
				var element = document.getElementById("date");
				if (easyread)
					element.value = day + "/" + month + "/" + year.substring(2) + " " + hour + ":" + oMinute + ":" + oSecond + " " + suffix;
				else
					element.value = year + "-" + month + "-" + day + " " + hour + ":" + oMinute + ":" + oSecond;
			}
			
			
			function procNew(useField)
			{
				elementTitle = document.getElementById("title");
				if (!useField || elementTitle.value == "")
					elementTitle.value = "NewPage";
				
				setDateTime(false, false);
				
				document.getElementById("mainForm").action = "editpage.php?pageid=-1";
				document.getElementById("editAction").value = "new";	
			}
			
			function procDelete(pageid)
			{
				if (pageid.length == 0)
				{
					var tally = "";
					var numTally = 0;
					for (var i=0; i < checkboxes.length; i++)
					{
						if (document.getElementById("Checkbox_" + checkboxes[i]).checked)
						{
							tally = tally + checkboxes[i] + ",";
							numTally ++;
						}
					}
					
					if (numTally == 0)
					{
						alert("No pages selected!");
						return;
					}
					else
					{
						if (!confirm("Are you sure you want to delete these " + numTally + " Pages?"))
							return;
							
						document.getElementById("editPageID").value = tally;
					}
				}
				else
				{
					if (!confirm("Are you sure you want to delete \"" + document.getElementById("LinkTitle_" + pageid).innerHTML + "\"?"))
						return;
					
					document.getElementById("editPageID").value = pageid;
				}
				
				document.getElementById("editAction").value = "delete";	
			}
			
			function procRename(pageid)
			{
				if (pageid.length == 0)
				{
					var tally = "";
					for (var i=0; i < checkboxes.length; i++)
					{
						if (document.getElementById("Checkbox_" + checkboxes[i]).checked)
							tally = tally + checkboxes[i] + ",";
					}
					
					if (tally.length == 0)
					{
						alert("No pages selected!");
						return;
					}
					else
						document.getElementById("editPageID").value = tally;
				}
				else
					document.getElementById("editPageID").value = pageid;
				
				document.getElementById("mainForm").action = "rename.php";
				document.getElementById("editAction").value = "rename";
			}
			
			
			function addCheckbox(pageid)
			{
				checkboxes[checkboxes.length] = pageid;
			}
			
			
			function showLinks(pageid)
			{
				window.open("<?php echo SITE_ROOT; ?>links.php?username=<?php echo $session->username; ?>&pageid=" + pageid, "WPLP", "toolbar=0, scrollbars=0, location=0, statusbar=0 ,menubar=0, resizable=0, width=300, height=140, left=" + ((window.screenX + window.outerWidth/2) - 150) + ", top=" + ((window.screenY + window.outerHeight/2) - 70));
			}
			
			function formVerify()
			{
				return (document.getElementById("editAction").value.length > 0);
			}
			
			
			function bodyResize()
			{
				for (var i = 0; i < dynamicTextboxes.length; i++)
				{
					if (!textbox.focused)
					{
						textbox = dynamicTextboxes[i];
						
						var maxwidth = textbox.container.clientWidth - textbox.buffer - 4;
						var width = textbox.desiredWidth;
						
						if (width > maxwidth)
							width = maxwidth;
						if (width < 10)
							width = 10;
						
						textbox.style.width = width + "px";
						textbox.width = width;
						
						textbox.span.style.fontSize = textbox.desiredFontSize + "em";
						textbox.span.innerHTML = textbox.value;
						
						var fontSize = textbox.desiredFontSize;
						var fontWidth = textbox.desiredWidth - 5;
						
						while (fontWidth > width && fontSize > 0.5)
						{
							fontSize -= 0.05;
							textbox.span.style.fontSize = fontSize + "em";
							fontWidth = textbox.span.offsetWidth + 3 + 2 * (fontSize / 1.5);
						}
						
						textbox.style.fontSize = fontSize + "em";
						textbox.fontSize = fontSize;
						textbox.span.innerHTML = "";
					}
				}
			}
			
			function dynamicTextboxOnCreate(textboxHTMLID, spanHTMLID, containerHTMLID, bufferHTMLID)
			{
				textbox = document.getElementById(textboxHTMLID);
				dynamicTextboxes[dynamicTextboxes.length] = textbox;
				
				textbox.span = document.getElementById(spanHTMLID);
				textbox.container = document.getElementById(containerHTMLID);//textbox.parentNode.clientWidth;
				
				textbox.style.width = "1px";
				textbox.buffer = document.getElementById(bufferHTMLID).offsetWidth;
				
				textbox.desiredFontSize = 1.0;
				dynamicTextboxOnBlur(textbox);
			}
			
			function dynamicTextboxOnFocus(textbox)
			{
				textbox.style.borderColor = '#ccc';
				textbox.style.backgroundColor = '#dfdfdf';
				textbox.style.width = '100%';
				//textbox.style.fontSize = textbox.desiredFontSize + "em";
				textbox.focused = true;
			}
			
			function dynamicTextboxOnBlur(textbox)
			{
				textbox.style.borderColor = 'transparent';
				textbox.style.backgroundColor = 'inherit';
				
				var maxwidth = textbox.container.clientWidth - textbox.buffer - 4;
				
				textbox.span.style.fontSize = textbox.desiredFontSize + "em";
				textbox.span.innerHTML = textbox.value;
				var width = textbox.span.offsetWidth + 10;
				textbox.desiredWidth = width;
				
				if (width > maxwidth)
					width = maxwidth;
				if (width < 10)
					width = 10;
				
				textbox.style.width = width + "px";
				textbox.width = width;
				
				var fontSize = textbox.desiredFontSize;
				var fontWidth = textbox.desiredWidth - 5;
				
				while (fontWidth > width && fontSize > 0.5)
				{
					fontSize -= 0.05;
					textbox.span.style.fontSize = fontSize + "em";
					fontWidth = textbox.span.offsetWidth + 3 + 2 * (fontSize / 1.5);
				}
				
				textbox.style.fontSize = fontSize + "em";
				textbox.fontSize = fontSize;
				
				textbox.span.innerHTML = '';
				textbox.focused = false;
				textbox.value = textbox.value;
			}
			
			function dynamicTextboxOnMouseOver(textbox)
			{
				if (!textbox.focused)
					textbox.style.borderColor = '#ccc';
			}
			
			function dynamicTextboxOnMouseOut(textbox)
			{
				if (!textbox.focused)
					textbox.style.borderColor = 'transparent';
			}
		</script>
    </head>
    <body onResize="bodyResize();">
        <div class="page">
            <div class="header">
                <div class="title">
                    <h1>
                        WebPad
                    </h1>
                </div>
                <div class="loginDisplay">
                    <?php
					if ($session->logged_in)
					{
                    ?>
                    	Welcome <b><?php echo $session->username; ?></b>! [ <a href="<?php echo SITE_ROOT; ?>Database/process.php">Log Out</a> ]
                    <?php
						if ($session->isAdmin())
						{
					?>
						[ <a href="<?php echo SITE_ROOT; ?>Database/admin.php">Admin Center</a> ]
                    <?php
						}
					}
					else
					{
					?>
                        [ <a href="<?php echo SITE_ROOT; ?>">Login</a> ]
                    <?php
					}
					?>
                </div>
                <div class="clear hideSkiplink">
                    <table cellpadding="0" cellspacing="0" border="0" class="menu">
                    	<tr>
                        	<td width="1">
                        		<a href="<?php echo SITE_ROOT; ?>">Home</a>
                            </td>
                            <td>
                            	<a href="<?php echo SITE_ROOT; ?>about.php">About</a></li>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="main">